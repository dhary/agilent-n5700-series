﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Examples" Type="Folder"/>
	<Item Name="Private" Type="Folder"/>
	<Item Name="Public" Type="Folder">
		<Item Name="N5700 Clear Status.vi" Type="VI" URL="../Public/N5700 Clear Status.vi"/>
		<Item Name="N5700 Close.vi" Type="VI" URL="../Public/N5700 Close.vi"/>
		<Item Name="N5700 Config Current Limit.vi" Type="VI" URL="../Public/N5700 Config Current Limit.vi"/>
		<Item Name="N5700 Config Current Protection.vi" Type="VI" URL="../Public/N5700 Config Current Protection.vi"/>
		<Item Name="N5700 Config Local-Remote.vi" Type="VI" URL="../Public/N5700 Config Local-Remote.vi"/>
		<Item Name="N5700 Config Output On-Off.vi" Type="VI" URL="../Public/N5700 Config Output On-Off.vi"/>
		<Item Name="N5700 Config Power-On State.vi" Type="VI" URL="../Public/N5700 Config Power-On State.vi"/>
		<Item Name="N5700 Config Voltage Limit.vi" Type="VI" URL="../Public/N5700 Config Voltage Limit.vi"/>
		<Item Name="N5700 Config Voltage Low Limit.vi" Type="VI" URL="../Public/N5700 Config Voltage Low Limit.vi"/>
		<Item Name="N5700 Config Voltage Protection.vi" Type="VI" URL="../Public/N5700 Config Voltage Protection.vi"/>
		<Item Name="N5700 CV-CC Query.vi" Type="VI" URL="../Public/N5700 CV-CC Query.vi"/>
		<Item Name="N5700 Error Query.vi" Type="VI" URL="../Public/N5700 Error Query.vi"/>
		<Item Name="N5700 Getting Started.vi" Type="VI" URL="../Public/N5700 Getting Started.vi"/>
		<Item Name="N5700 Initialize.vi" Type="VI" URL="../Public/N5700 Initialize.vi"/>
		<Item Name="N5700 Local-Remote Query.vi" Type="VI" URL="../Public/N5700 Local-Remote Query.vi"/>
		<Item Name="N5700 Meas Output Current.vi" Type="VI" URL="../Public/N5700 Meas Output Current.vi"/>
		<Item Name="N5700 Meas Output Voltage.vi" Type="VI" URL="../Public/N5700 Meas Output Voltage.vi"/>
		<Item Name="N5700 Model Query.vi" Type="VI" URL="../Public/N5700 Model Query.vi"/>
		<Item Name="N5700 On-Off Setting Query.vi" Type="VI" URL="../Public/N5700 On-Off Setting Query.vi"/>
		<Item Name="N5700 Protection Tripped Clear.vi" Type="VI" URL="../Public/N5700 Protection Tripped Clear.vi"/>
		<Item Name="N5700 Protection Tripped Query.vi" Type="VI" URL="../Public/N5700 Protection Tripped Query.vi"/>
		<Item Name="N5700 Protections Query.vi" Type="VI" URL="../Public/N5700 Protections Query.vi"/>
		<Item Name="N5700 Read Instrument Data.vi" Type="VI" URL="../Public/N5700 Read Instrument Data.vi"/>
		<Item Name="N5700 Recall Configuration.vi" Type="VI" URL="../Public/N5700 Recall Configuration.vi"/>
		<Item Name="N5700 Reset.vi" Type="VI" URL="../Public/N5700 Reset.vi"/>
		<Item Name="N5700 Revision Query.vi" Type="VI" URL="../Public/N5700 Revision Query.vi"/>
		<Item Name="N5700 Save Configuration.vi" Type="VI" URL="../Public/N5700 Save Configuration.vi"/>
		<Item Name="N5700 VI Tree.vi" Type="VI" URL="../Public/N5700 VI Tree.vi"/>
		<Item Name="N5700 Voltage-Current Limits Query.vi" Type="VI" URL="../Public/N5700 Voltage-Current Limits Query.vi"/>
		<Item Name="N5700 Wait.vi" Type="VI" URL="../Public/N5700 Wait.vi"/>
		<Item Name="N5700 Write to Instrument.vi" Type="VI" URL="../Public/N5700 Write to Instrument.vi"/>
	</Item>
	<Item Name="Agilent N5700 Readme.htm" Type="Document" URL="../Agilent N5700 Readme.htm"/>
</Library>
